function loadAllStudents() {
    var responseData = null;
    var data = fetch('https://dv-student-backend-2019.appspot.com/students')
        .then((response) => {
            console.log(response)
            //console.log(response.json())
            return response.json()
        }).then((json) => {
            responseData = json
            console.log(json)
            var resultElement = document.getElementById('result')
            resultElement.innerHTML = JSON.stringify(responseData, null, 2)
        })
}

async function loadAllStudentsAsync() {
    let response = await fetch('https://dv-student-backend-2019.appspot.com/students')
    let data = await response.json()
    // var resultElement = document.getElementById('result')
    // resultElement.innerHTML = JSON.stringify(data, null, 2)
    return data
}

function createResultTable(data) {
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'studentId'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'surname'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'gpa'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)


    console.log(data)

    data.then((json) => {
        for (let i = 0; i < json.length; i++) {
            var currentData = json[i]
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)

            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope', 'row')
            dataFirstColumnNode.innerText = currentData['id']
            dataRow.appendChild(dataFirstColumnNode)
            var columnNode = null;

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['studentId']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['name']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['surname']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')
            columnNode.innerText = currentData['gpa']
            dataRow.appendChild(columnNode)

            columnNode = document.createElement('td')

            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['image'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)
        }
    })
}

function CreateOneStudentTable(data) {

    let resultElement = document.getElementById('resultOneStudent')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)

    var tableRowNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRowNode)

    var tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'studentId'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'name'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'surname'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'gpa'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    console.log(data)

    data.then((obj) => {

        var currentData = obj
        var dataRow = document.createElement('tr')
        tableNode.appendChild(dataRow)

        var dataFirstColumnNode = document.createElement('th')
        dataFirstColumnNode.setAttribute('scope', 'row')
        dataFirstColumnNode.innerText = currentData['id']
        dataRow.appendChild(dataFirstColumnNode)
        var columnNode = null;

        columnNode = document.createElement('td')
        columnNode.innerText = currentData['studentId']
        dataRow.appendChild(columnNode)

        columnNode = document.createElement('td')
        columnNode.innerText = currentData['name']
        dataRow.appendChild(columnNode)

        columnNode = document.createElement('td')
        columnNode.innerText = currentData['surname']
        dataRow.appendChild(columnNode)

        columnNode = document.createElement('td')
        columnNode.innerText = currentData['gpa']
        dataRow.appendChild(columnNode)

        columnNode = document.createElement('td')

        var imageNode = document.createElement('img')
        imageNode.setAttribute('src', currentData['image'])
        imageNode.style.width = '200px'
        imageNode.style.height = '200px'
        dataRow.appendChild(imageNode)
    })

}

async function loadOneStudent() {
    let studentId = document.getElementById('queryId').value
    if (studentId != '' && studentId != null) {
        let response = await fetch('https://dv-student-backend-2019.appspot.com/students/' + studentId)
        let data = await response.json()
        //console.log(data)
        return data
    }
}